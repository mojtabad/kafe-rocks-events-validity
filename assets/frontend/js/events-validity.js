"use strict";

(function ($) {
  jQuery(document).ready(function ($) {

    $(document).on('click', '.more_button_row a', function (event) {
      event.preventDefault();
      $(this).parents('table').find('.hidden_row').removeClass('hidden_row');
      $(this).parents('tr').remove();
    });

    $(document).on('click', '.events-validity-block th.sortable_th', function (event) {
      var table = $(this).parents('table').eq(0)
      var rows = table.find('tr:gt(0)').not('.more_button_row').toArray().sort(comparer($(this).index()))
      this.asc = !this.asc
      if (!this.asc) {
        rows = rows.reverse()
      }
      for (var i = 0; i < rows.length; i++) {
        table.prepend(rows[i])
      }
    })

    function comparer(index) {
      return function (a, b) {
        var valA = getCellValue(a, index), valB = getCellValue(b, index)
        return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.toString().localeCompare(valB)
      }
    }

    function getCellValue(row, index) {
      return $(row).children('td').eq(index).data('hours')
    }

  });
})(jQuery);

