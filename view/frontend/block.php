<?php
/**
 * @var array $block
 * @var string $content
 * @var bool $is_preview
 * @var int|string $post_id
 * @var string $class_name
 * @var string $events_column_header
 * @var string $validity_hour_column_header
 * @var array $events
 * @var bool $use_default_style
 *
 */
?>

<div class="<?php echo esc_attr( $class_name ); ?>" id="<?php echo esc_attr( $block['id'] ); ?>">
    <table <?php echo $use_default_style ? 'class="styled"' : '' ?>>
        <thead>
        <tr>
            <th><?php echo esc_attr( $events_column_header ) ?></th>
            <th class="sortable_th"><span class="sort-by"><?php echo esc_attr( $validity_hour_column_header ) ?></span>
            </th>
        </tr>
        </thead>
        <tbody>
		<?php foreach ( $events as $index => $event ): ?>
            <tr <?php echo $index >= 3 ? 'class="hidden_row"' : '' ?>>
                <td data-label="<?php echo esc_attr( $events_column_header ) ?>">
                    <i class="<?php echo esc_attr( $event['icon'] ) ?>"></i>
					<?php echo $event['description'] ?>
                </td>
                <td data-label="<?php echo esc_attr( $validity_hour_column_header ) ?>"
                    data-hours="<?php echo $event['validity_time'] ?: 0 ?>">
					<?php
					if ( $event['validity_time'] ) {
						$hours = $event['validity_time'];
						$days  = floor( $hours / 24 );
						if ( $days > 0 ) {
							$text = sprintf( __( 'Up to %d days', 'events-validity' ), $days );
						} else {
							$text = sprintf( __( 'Up to %d hours', 'events-validity' ), $hours );
						}
					} else {
						$text = 'N/A';
					}

					echo $text;
					?>
                </td>
            </tr>
		<?php endforeach; ?>

		<?php if ( count( $events ) > 3 ) : ?>
            <tr class="more_button_row">
                <td colspan="2">
                    <a href="#"><?php _e( 'See more', 'events-validity' ); ?></a>
                </td>
            </tr>
		<?php endif; ?>
        </tbody>
    </table>
</div>