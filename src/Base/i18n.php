<?php


namespace KafeRocks\EventsValidity\Base;


/**
 * Class i18n
 * @package KafeRocks\EventsValidity\Base
 */
class i18n {

	/**
	 * i18n constructor.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {
		add_action( 'init', [ $this, 'load' ] );
	}

	/**
	 * Load plugin text domain.
	 *
	 * @since 1.0.0
	 */
	public static function load() {
		$plugin_rel_path = plugin_basename( EVENTS_VALIDITY_PATH ) . '/languages';
		load_plugin_textdomain( 'events-validity', false, $plugin_rel_path );
	}

}