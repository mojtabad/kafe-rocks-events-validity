<?php

namespace KafeRocks\EventsValidity\Base;

/**
 * Class Assets
 * @package KafeRocks\EventsValidity\Base
 */
class Assets {
	/**
	 * Assets constructor.
	 */
	public function __construct() {
		add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_scripts' ] );
		add_action( 'admin_enqueue_scripts', [ $this, 'admin_scripts' ] );
	}

	/**
	 * Enqueue plugin assets in website frontend
	 *
	 * @since 1.0.0
	 */
	public function enqueue_scripts() {
		wp_enqueue_style( 'block-events-font-awesome', EVENTS_VALIDITY_URL . 'assets/frontend/css/font-awesome.css', [], '5.15.4' );
	}

	/**
	 * Enqueue plugin assets in website backend
	 *
	 * @since 1.0.0
	 */
	public function admin_scripts() {
		wp_enqueue_style( 'block-events-font-awesome', EVENTS_VALIDITY_URL . 'assets/frontend/css/font-awesome.css', [], '5.15.4' );
	}
}