<?php

namespace KafeRocks\EventsValidity;

use KafeRocks\EventsValidity\Base\Assets;
use KafeRocks\EventsValidity\Base\i18n;
use KafeRocks\EventsValidity\Block\AcfFieldsGroup;
use KafeRocks\EventsValidity\Block\AcfFontAwesomeField;
use KafeRocks\EventsValidity\Block\Register as Block;
use League\Container\Container;

final class Bootstrap {

	/**
	 * The single instance of the class.
	 *
	 * @var Bootstrap
	 * @since 1.0.0
	 */
	private static $_instance;

	/**
	 * The Dependency Injection Container
	 *
	 * @var Container
	 * @since 1.0.0
	 */
	private $container;

	/**
	 * Bootstrap constructor.
	 */
	private function __construct() {
		if ( class_exists( 'ACF' ) ) {
			$this->container = new Container();
			$this->container->defaultToShared();

			$this->init();
		} else {
			add_action( 'admin_notices', [ $this, 'show_acf_activation_notice' ] );
		}
	}

	/**
	 * Instantiate plugin's classes using container
	 *
	 * @since 1.0.0
	 */
	public function init() {
		$this->container->add( i18n::class )->addTag( 'base' );
		$this->container->add( Assets::class )->addTag( 'base' );
		$this->container->get( 'base' );

		$this->container->add( Block::class )->addTag( 'block' );
		$this->container->add( AcfFieldsGroup::class )->addTag( 'block' );
		$this->container->add( AcfFontAwesomeField::class )->addTag( 'block' );
		$this->container->get( 'block' );
	}

	/**
	 * Show the ACF Pro plugin missing admin notice
	 *
	 * @since 1.0.0
	 */
	public function show_acf_activation_notice() {
		$class   = 'notice notice-error';
		$message = __( 'In order to use events validity plugin you have to install ACF Pro plugin.', 'events-validity' );

		printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) );
	}


	/**
	 * Main Class Instance.
	 *
	 * Ensures only one instance of this class is loaded or can be loaded.
	 *
	 * @static
	 * @return self - Main instance.
	 */
	public static function instance(): Bootstrap {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/**
	 * Get DI container
	 *
	 * @return Container
	 */
	public function get_container(): Container {
		return $this->container;
	}
}