<?php

namespace KafeRocks\EventsValidity\Block;

class AcfFieldsGroup {
	/**
	 * AcfFieldsGroup constructor
	 */
	public function __construct() {
		if ( function_exists( 'acf_add_local_field_group' ) ) {
			add_action( 'init', [ $this, 'add_local_field_group' ] );
		}
	}


	/**
	 * Add ACF fields to registered gutenberg block
	 */
	public function add_local_field_group() {
		acf_add_local_field_group( [
			'key'                   => 'group_613b7effedc45',
			'title'                 => __( 'Events validity', ' events-validity' ),
			'fields'                => [
				[
					'key'               => 'field_613b7f0d74244',
					'label'             => __( 'Events column header', ' events-validity' ),
					'name'              => 'events_column_header',
					'type'              => 'text',
					'instructions'      => '',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => [
						'width' => '',
						'class' => '',
						'id'    => '',
					],
					'default_value'     => '',
					'placeholder'       => '',
					'prepend'           => '',
					'append'            => '',
					'maxlength'         => '',
				],
				[
					'key'               => 'field_613b7f1d74245',
					'label'             => __( 'Validity hour column header', ' events-validity' ),
					'name'              => 'validity_hour_column_header',
					'type'              => 'text',
					'instructions'      => '',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => [
						'width' => '',
						'class' => '',
						'id'    => '',
					],
					'default_value'     => '',
					'placeholder'       => '',
					'prepend'           => '',
					'append'            => '',
					'maxlength'         => '',
				],
				[
					'key'               => 'field_613b7f2d74246',
					'label'             => __( 'Use default style', ' events-validity' ),
					'name'              => 'use_default_style',
					'type'              => 'true_false',
					'instructions'      => '',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => [
						'width' => '',
						'class' => '',
						'id'    => '',
					],
					'message'           => 'Yes',
					'default_value'     => 1,
					'ui'                => 0,
					'ui_on_text'        => '',
					'ui_off_text'       => '',
				],
				[
					'key'               => 'field_613b7f4474247',
					'label'             => __( 'Events', ' events-validity' ),
					'name'              => 'events',
					'type'              => 'repeater',
					'instructions'      => '',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => [
						'width' => '',
						'class' => '',
						'id'    => '',
					],
					'collapsed'         => '',
					'min'               => 1,
					'max'               => 10,
					'layout'            => 'block',
					'button_label'      => '',
					'sub_fields'        => [
						[
							'key'               => 'field_613b7f5a74248',
							'label'             => __( 'Icon', ' events-validity' ),
							'name'              => 'icon',
							'type'              => 'select',
							'instructions'      => '',
							'required'          => 0,
							'conditional_logic' => 0,
							'wrapper'           => [
								'width' => '',
								'class' => '',
								'id'    => '',
							],
							'choices'           => [],
							'default_value'     => false,
							'allow_null'        => 0,
							'multiple'          => 0,
							'ui'                => 1,
							'ajax'              => 0,
							'return_format'     => 'value',
							'placeholder'       => '',
						],
						[
							'key'               => 'field_613b7f6a74249',
							'label'             => __( 'Description', ' events-validity' ),
							'name'              => 'description',
							'type'              => 'wysiwyg',
							'instructions'      => '',
							'required'          => 1,
							'conditional_logic' => 0,
							'wrapper'           => [
								'width' => '',
								'class' => '',
								'id'    => '',
							],
							'default_value'     => '',
							'tabs'              => 'all',
							'toolbar'           => 'full',
							'media_upload'      => 1,
							'delay'             => 0,
						],
						[
							'key'               => 'field_613b7f767424a',
							'label'             => __( 'Validity time', ' events-validity' ),
							'name'              => 'validity_time',
							'type'              => 'number',
							'instructions'      => '',
							'required'          => 0,
							'conditional_logic' => 0,
							'wrapper'           => [
								'width' => '',
								'class' => '',
								'id'    => '',
							],
							'default_value'     => '',
							'placeholder'       => '',
							'prepend'           => '',
							'append'            => '',
							'min'               => 0,
							'max'               => '',
							'step'              => 1,
						],
					],
				],
			],
			'location'              => [
				[
					[
						'param'    => 'block',
						'operator' => '==',
						'value'    => 'acf/events-validity',
					],
				],
			],
			'menu_order'            => 0,
			'position'              => 'normal',
			'style'                 => 'default',
			'label_placement'       => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen'        => '',
			'active'                => true,
			'description'           => '',
		] );

	}

}