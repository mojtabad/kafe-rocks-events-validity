<?php

namespace KafeRocks\EventsValidity\Block;

use KafeRocks\EventsValidity\Helpers\View;

class Register {

	/**
	 * Register constructor.
	 */
	public function __construct() {
		add_action( 'init', [ $this, 'register' ] );
	}

	/**
	 * Register gutenberg block using acf_register_block_type method
	 */
	public function register() {
		if ( function_exists( 'acf_register_block_type' ) ) {
			acf_register_block_type( [
				'name'            => 'events-validity',
				'title'           => __( 'Events validity', 'events-validity' ),
				'description'     => __( 'Events validity block', 'events-validity' ),
				'render_callback' => [ $this, 'render' ],
				'enqueue_assets'  => [ $this, 'enqueue_assets' ],
				'category'        => 'common',
				'icon'            => [
					'background' => '#ed1b2f',
					'foreground' => '#fff',
					'src'        => 'flag',
				],
				'keywords'        => [ 'events' ],
			] );
		}
	}

	/**
	 * Block render
	 *
	 * @param array $block The block settings and attributes.
	 * @param string $content The block inner HTML (empty).
	 * @param bool $is_preview True during AJAX preview.
	 * @param int|string $post_id The post ID this block is saved to.
	 */
	public function render( array $block, string $content = '', bool $is_preview = false, $post_id = 0 ) {
		// Create class attribute allowing for custom "className".
		$class_name = 'events-validity-block';
		if ( ! empty( $block['className'] ) ) {
			$class_name .= ' ' . $block['className'];
		}

		$events_column_header        = get_field( 'events_column_header' ) ?: __( 'Event', 'events-validity' );
		$validity_hour_column_header = get_field( 'validity_hour_column_header' ) ?: __( 'Validity', 'events-validity' );
		$events                      = get_field( 'events' ) ?: [];
		$use_default_style           = get_field( 'use_default_style' );

		View::get( 'block', 'frontend', [
			'block'                       => $block,
			'content'                     => $content,
			'is_preview'                  => $is_preview,
			'post_id'                     => $post_id,
			'class_name'                  => $class_name,
			'events_column_header'        => $events_column_header,
			'validity_hour_column_header' => $validity_hour_column_header,
			'events'                      => $events,
			'use_default_style'           => $use_default_style,
		] );
	}

	/**
	 * Enqueue block css and js file
	 *
	 */
	public function enqueue_assets() {
		// Enqueue CSS
		wp_enqueue_style( 'block-events-validity', EVENTS_VALIDITY_URL . 'assets/frontend/css/events-validity.css', [], EVENTS_VALIDITY_VERSION );

		// Enqueue JS
		wp_enqueue_script( 'block-events-validity', EVENTS_VALIDITY_URL . 'assets/frontend/js/events-validity.js', [ 'jquery' ], EVENTS_VALIDITY_VERSION, true );
	}

}