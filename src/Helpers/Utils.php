<?php

namespace KafeRocks\EventsValidity\Helpers;

use League\Container\Container;
use KafeRocks\EventsValidity\Bootstrap;

/**
 * Class Utils
 * @package KafeRocks\EventsValidity\Helpers
 */
class Utils {

	/**
	 * Get plugin's container
	 *
	 * @return Container
	 */
	public static function get_container(): Container {
		return Bootstrap::instance()->get_container();
	}

}