<?php

namespace KafeRocks\EventsValidity\Helpers;


class View {
	/**
	 * Render a view template
	 *
	 * @param string $template_name
	 * @param string $template_path
	 * @param array $args
	 */
	public static function get( string $template_name, string $template_path = '', array $args = [] ) {
		if ( ! empty( $args ) && is_array( $args ) ) {
			extract( $args );
		}

		$path = sprintf( '%s/view/', EVENTS_VALIDITY_PATH );
		if ( ! empty( $template_path ) ) {
			$path .= sprintf( '%s/', $template_path );
		}

		$template = sprintf( '%s%s.php', $path, $template_name );

		if ( file_exists( $template ) ) {
			include $template;
		}
	}

}