=== Plugin Name ===
Contributors: mojtabad
Requires at least: 5
Tested up to: 5.7
Requires PHP: 7.2

This is a gutenberg block plugin for KafeRocks test

== Description ==

 Gutenberg block to display an events table.

== Installation ==

1. Upload `events-validity-gutenberg-block` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress


== Development notes ==

This plugin uses the PSR-4 standard for Auto loading its classes. Also, it uses the WordPress coding standard for naming methods.
The php league container is used as the PSR-11 dependency injection container.
For more information:
https://php-fig.org/psr/psr-4/
https://php-fig.org/psr/psr-11/
https://container.thephpleague.com/
