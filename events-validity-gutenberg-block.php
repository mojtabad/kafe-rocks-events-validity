<?php
/*
Plugin Name: Events validity gutenberg block
Plugin URI: https://kafe.rocks/
Description: Gutenberg block to display an events table
Author: Mojtaba Darvishi
Version: 1.0.0
Author URI: https://kafe.rocks/
Text Domain: events-validity
Domain Path: /languages/
Requires at least: 5.7
Requires PHP: 7.4
*/

defined( 'ABSPATH' ) || exit;

define( 'EVENTS_VALIDITY_PATH', realpath( plugin_dir_path( __FILE__ ) ) );
define( 'EVENTS_VALIDITY_URL', plugin_dir_url( __FILE__ ) );
const EVENTS_VALIDITY_VERSION = '1.0.0';


require __DIR__ . '/src/Autoloader.php';
KafeRocks\EventsValidity\Autoloader::init();

if ( ! class_exists( 'KafeRocks\EventsValidity\Bootstrap', false ) ) {
	include_once EVENTS_VALIDITY_PATH . '/src/Bootstrap.php';
}
KafeRocks\EventsValidity\Bootstrap::instance();
